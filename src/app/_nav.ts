export const navigation = [
  {
    title: true,
    name: 'Main Menu'
  },
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Courses',
    url: '/courses',
    icon: 'icon-book-open',
    children: [
      {
        name: 'CoursesCurrent',
        url: '/courses',
        icon: 'icon-book-open',
      },
      {
        name: 'CoursesHistory',
        url: '/courses/history',
        icon: 'icon-book-open',
      },
    ]
  },
  {
    name: 'Projects',
    url: '/projects',
    icon: 'icon-note',
    children: [
      {
        name: 'ProjectsCurrent',
        url: '/projects/current',
        icon: 'icon-note',
      },
      {
        name: 'ProjectsCompleted',
        url: '/projects/completed',
        icon: 'icon-note',
      },
    ]
  },
  {
    name: 'Students',
    url: '/students',
    icon: 'icon-user'
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'User Menu',
  },
  {
    name: 'ProfileTitle',
    url: '/profile',
    icon: 'icon-user',

  },
  {
    name: 'Languageslabel',
    url: '/lang',
    icon: 'icon-globe',
    children: [
      {
        name: 'Ελληνικά',
        url: '/lang/el',
        icon: 'icon-cursor'
      },
      {
        name: 'English',
        url: '/lang/en',
        icon: 'icon-cursor'
      }
    ]

  },
  {
    name: 'Logout',
    url: '/auth/logout',
    icon: 'icon-lock',
    variant: 'notice'
  }
];
