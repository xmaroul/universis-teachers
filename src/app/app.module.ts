import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {environment} from '../environments/environment';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AngularDataContext, ClientDataContextConfig, DATA_CONTEXT_CONFIG} from "@themost/angular";
import {ConfigurationService} from "./shared/services/configuration.service";
import {SharedModule} from "./shared/shared.module";
import {ErrorModule} from "./error/error.module";
import {AuthModule} from './auth/auth.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ChartsModule } from 'ng2-charts/ng2-charts';



// Routing Module
import {AppRoutingModule} from './app.routing';
import {BreadcrumbsComponent} from "./layouts/breadcrumb.component";
import { ProfileService } from './profile/services/profile.service';

// Import coreui components
import {
//  AppAsideComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './shared/coreui/components';

// Import coreui directives
import {
//  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './shared/coreui/directives';
import { ProjectsModule } from './projects/projects.module';
import { StudentsModule } from './students/students.module';

const APP_COMPONENTS = [
//  AppAsideComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
];

const APP_DIRECTIVES = [
//  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
];

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    BreadcrumbsComponent,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule,
    AuthModule,
    AppRoutingModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ProjectsModule,
    StudentsModule

  ],
  providers: [
    {
            provide: DATA_CONTEXT_CONFIG, useValue: {
              base: "/",
              options: {
                  useMediaTypeExtensions: false
              }
            }
    },
    AngularDataContext,
    {
            provide: APP_INITIALIZER,
            useFactory: (config: ConfigurationService) => () => config.load(),
            deps: [ConfigurationService],
            multi: true
    },
    {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
    },
    ProfileService
    ],
  bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(private _translateService: TranslateService, private _configurationService: ConfigurationService) {
      //
    }
}
