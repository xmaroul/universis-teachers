import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-msgbox',
  templateUrl: './msgbox.component.html',
  styleUrls: ['./msgbox.component.scss']
})
export class MsgboxComponent implements OnInit {

  @Input() icon: string;
  @Input() info: string;
  @Input() message: string;
  @Input() extraMessage: string;
  // ActionButton Array {link: "", text: ""}
  @Input() actionButton: any;
  @Input() actionText: string;

  constructor() { }

  ngOnInit() {  }

   openLink() {
    window.open(this.actionButton.link, '_self');
   }

}
