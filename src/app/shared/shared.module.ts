import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import {ConfigurationService} from "./services/configuration.service";
import { MsgboxComponent } from './msgbox/msgbox.component';
import { LangswitchComponent } from './langswitch/langswitch.component';
import {LocalizedDatePipe} from "./localized-date.pipe";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {LangComponent} from "./lang-component";
import {SpinnerComponent} from './modals/spinner.component';
import { NgSpinKitModule } from 'ng-spin-kit';
import {ModalService} from './services/modal.service';
import {ModalModule} from 'ngx-bootstrap';
import {LoadingService} from './services/loading.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    BsDropdownModule,
    ModalModule,
    NgSpinKitModule
  ],
  providers: [
    ConfigurationService,
    ModalService,
    LoadingService
  ],
  declarations: [
    LocalizedDatePipe,
    MsgboxComponent,
    LangComponent,
    LangswitchComponent,
    SpinnerComponent
  ],
  entryComponents: [
    SpinnerComponent
  ],
  exports: [
    MsgboxComponent,
    LangswitchComponent,
    LangComponent,
    LocalizedDatePipe]
})
export class SharedModule { }
