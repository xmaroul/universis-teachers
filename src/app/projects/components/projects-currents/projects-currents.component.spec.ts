import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsCurrentsComponent } from './projects-currents.component';

describe('ProjectsCurrentsComponent', () => {
  let component: ProjectsCurrentsComponent;
  let fixture: ComponentFixture<ProjectsCurrentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsCurrentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsCurrentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
