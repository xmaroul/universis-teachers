import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private _context: AngularDataContext) { }

  getCompletedProjects(): any {
    return this._context.model('Instructors/Me/theses')
      .asQueryable()
      .expand('type,students($expand=student($expand=person,studentStatus)),status')
      .where('status/alternateName').equal('completed')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .getItems();
  }

  getCurrentProjects(): any {
    return this._context.model('Instructors/Me/activetheses')
      .asQueryable()
      .expand('type,students($expand=student($expand=person,studentStatus)),status')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .getItems();
  }

}
