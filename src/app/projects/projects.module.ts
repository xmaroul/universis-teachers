import { NgModule } from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { ProjectsCompletedComponent } from './components/projects-completed/projects-completed.component';
import { ProjectsCurrentsComponent } from './components/projects-currents/projects-currents.component';

import {NgPipesModule} from 'ngx-pipes';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ProjectsRoutingModule,
    TranslateModule,
    NgPipesModule,
    FormsModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  declarations: [ProjectsCompletedComponent, ProjectsCurrentsComponent]
})

export class ProjectsModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/projects.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
