import {Component, HostListener, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {NavigationEnd, Router} from '@angular/router';
import {CoursesService} from '../../services/courses.service';
import {LoadingService} from '../../../shared/services/loading.service';
import {ErrorService} from '../../../error/error.service';


@Component({
  selector: 'app-courses-history',
  templateUrl: './courses-history.component.html',
  styleUrls: ['./courses-history.component.scss'],
  providers: [ CoursesService ]
})
export class CoursesHistoryComponent implements OnInit {

  public courseClasses;
  public selectedIndex = 0;
  public isSrolling = true;
  public loading = true;

  constructor(private _context: AngularDataContext,
              private coursesService: CoursesService,
              private errorService: ErrorService,
              private router: Router,
              private loadingService: LoadingService) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      }
    });
    this.loadingService.showLoading();
    this.coursesService.getAllClasses().then((res) => {
      this.courseClasses = res.value;
      this.loadingService.hideLoading();
      this.loading = false;
    }).catch(err => {
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });
  }


  ngOnInit() {
  }
  setIndex(index: number) {
    this.selectedIndex = index;
    this.isSrolling = false;
  }
  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    return this.getScroll();
  }

  public getScroll( node = document.getElementById('inner__content') ): number {
    if ( !this.isSrolling ) {
      this.isSrolling = true;
      return null;
    }

    const currentScroll = node.scrollTop;
    const elements = document.getElementsByClassName('years');
    let fountActive = false;

    this.selectedIndex = -1;

    for (let j = 0; j < elements.length; j++) {
      const currentElement = elements.item(j);

      let currentOffSetTop = currentElement.getBoundingClientRect().top;
      if (currentOffSetTop < 0) {
        currentOffSetTop *= -1;
      }

    if (((currentElement.getBoundingClientRect().top > 0) || (currentElement.getBoundingClientRect().height - 10) >
        (node.getBoundingClientRect().top + currentOffSetTop)) && !fountActive) {

        this.selectedIndex = j;
        console.log(this.selectedIndex);

        fountActive = true;
      }
    }
    return( currentScroll );
  }
}

