import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class ProfileService {

  constructor( private context: AngularDataContext) { }

  getInstructor() {
    return this.context.model('instructors/me')
      .asQueryable()
      .expand('department($expand=currentYear,currentPeriod)' )
      .getItem().then(result => {
          if (typeof result === 'undefined') {
            return Promise.reject('Student data cannot be found');
          }
          return Promise.resolve(result);
      }).catch( err => {
        if (err.status === 404) {
          // throw 401 Unauthorized
          return Promise.reject(Object.assign(new HttpErrorResponse({
            error: new Error('Instructor cannot be found or is inaccessible'),
            status: 401
          }), {
            // assign continue as for auto logout
            continue: '/auth/loginAs'
          }));
        }
        return Promise.reject(err);
      });

  }

}
